package foundation.e.lib.telemetry

internal object Constants {
    const val SETTINGS_TELEMETRY_FIELD = "e_telemetry"
    const val SENTRY_USERID = "sentry_userid";
    const val TAG_E_VERSION = "e_version"
    const val PROPERTY_OS_VERSION = "ro.lineage.version"
}
