package foundation.e.lib.telemetry

import android.annotation.SuppressLint
import android.app.Application
import android.provider.Settings
import foundation.e.lib.telemetry.Constants.SETTINGS_TELEMETRY_FIELD
import foundation.e.lib.telemetry.Telemetry.isReportingTelemetry
import io.sentry.Sentry
import io.sentry.SentryLevel
import io.sentry.android.core.SentryAndroid
import io.sentry.android.timber.SentryTimberIntegration
import io.sentry.protocol.User

object Telemetry {

    private var identifier: String = ""
    private lateinit var application: Application
    private var enableOsTag: Boolean = true
    private var isTelemetryEnabled: Boolean = false
    private var userSentryId: String = ""

    /**
     * Call this function in `onCreate()` of custom Application class.
     * Telemetry will be enabled only if enabled from developer options.
     *
     * @param identifier: the DSN identifier of the Sentry project
     * @param application: the application context
     */
    @JvmStatic
    fun init(identifier: String, application: Application, enableOsTag: Boolean = true) {
        this.identifier = identifier
        this.application = application
        this.enableOsTag = enableOsTag
        this.isTelemetryEnabled = checkTelemetryDeveloperOption()
        this.userSentryId = getSentryUserId(application)

        val sentryDsn =
            if (isTelemetryEnabled) {
                identifier
            } else {
                String()
            }

        try {
            SentryAndroid.init(application) { options ->
                options.dsn = sentryDsn
                options.addIntegration(
                    SentryTimberIntegration(
                        minEventLevel = SentryLevel.ERROR,
                        minBreadcrumbLevel = SentryLevel.INFO
                    )
                )
            }

            Sentry.configureScope {
                if (enableOsTag) {
                    val eVersion = getSystemProperty(Constants.PROPERTY_OS_VERSION) ?: ""
                    it.setTag(Constants.TAG_E_VERSION, eVersion)
                } else {
                    it.removeTag(Constants.TAG_E_VERSION)
                }
                it.user = User().apply {
                    id = userSentryId
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Send a simple string message.
     *
     * @param message: the message to send
     */
    @JvmStatic
    fun reportMessage(message: String) {
        Sentry.captureMessage(message)
    }

    /**
     * Send a simple string message.
     * Specify the level of the message.
     *
     * @param message: the message to send
     * @param level: the level of the message
     */
    @JvmStatic
    fun reportMessage(message: String, level: SentryLevel) {
        Sentry.captureMessage(message, level)
    }

    /**
     * Report an exception.
     * Generally called from catch block of try-catch.
     *
     * @param e: The exception to report.
     */
    @JvmStatic
    fun reportException(e: Exception) {
        Sentry.captureException(e)
    }

    @Deprecated(
        "Use isReportingTelemetry()",
        ReplaceWith("isReportingTelemetry()"),
    )
    @JvmStatic
    fun isTelemetryEnabled(application: Application): Boolean {
        return try {
            Settings.System.getInt(application.contentResolver, SETTINGS_TELEMETRY_FIELD) == 1
        } catch (e: Settings.SettingNotFoundException) {
            false
        } catch (_: Exception) {
            false
        }
    }

    private fun getSentryUserId(application: Application): String {
        return try {
            Settings.Secure.getString(application.contentResolver, Constants.SENTRY_USERID)
        } catch (e: Settings.SettingNotFoundException) {
            ""
        } catch (_: Exception) {
            ""
        }
    }

    /**
     * It is possible that the telemetry option in developer options is different from
     * the current state of the library. It is possible if the app is open and
     * e_telemetry system setting is (say) disabled via ADB command.
     *
     * This method reports the actual state of telemetry reporting, immaterial of the
     * developer options. This state is set when [init] has been called, usually
     * during app start up.
     */
    @JvmStatic
    fun isReportingTelemetry() = isTelemetryEnabled

    /**
     * Return a unique id for a device, that can be used for sentry.
     */
    @JvmStatic
    fun getSentryUserId() = userSentryId

    /**
     * Read from OS developer options.
     * Pass false by default.
     */
    private fun checkTelemetryDeveloperOption(): Boolean {
        return try {
            Settings.System.getInt(application.contentResolver, SETTINGS_TELEMETRY_FIELD) == 1
        } catch (e: Settings.SettingNotFoundException) {
            false
        } catch (_: Exception) {
            false
        }
    }

    @SuppressLint("PrivateApi")
    fun getSystemProperty(key: String): String? {
        var value: String? = null
        try {
            value = Class.forName("android.os.SystemProperties")
                .getMethod("get", String::class.java).invoke(null, key) as String
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return value
    }
}
