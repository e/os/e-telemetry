# Telemetry

Wrapper for Sentry to use in /e/ OS default applications.

## Usage

- First, we need to add dependency repository to `settings.gradle`:

```groovy
maven("https://gitlab.e.foundation/api/v4/groups/1391/-/packages/maven")
```

- Then, we need to add dependency to `build.gradle`:

```groovy
implementation 'foundation.e.lib:telemetry:0.0.10-alpha'
```

- Get the Sentry DSN from the sentry dashboard and add the following to `build.gradle`:

```groovy
buildTypes {
    configureEach {
        buildConfigField("String", "SENTRY_DSN", "\"${System.getenv("SENTRY_DSN")}\"")
    }
}
```

- To use the library, we need to add the following code to the main `Application` class:

```kotlin
Telemetry.init(BuildConfig.SENTRY_DSN, this)
```

- To send an event, we need to add the following code:

```kotlin
Telemetry.reportMessage("sample message")
```

- To get sentry user id, we need to add the following code:

```kotlin
Telemetry.getSentryUserId()
```

# How to force telemetry to be enabled without manual action in settings?

Use adb command:

`adb shell settings put system e_telemetry 1`

# How to get/change sentry user id?

Use adb command:

`adb shell settings get secure sentry_userid`
`adb shell settings put secure sentry_userid 2163681a-0980-41f6-8044-7db50c580986`
